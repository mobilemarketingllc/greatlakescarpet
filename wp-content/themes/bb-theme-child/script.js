
// header flyer choose location js start here 
jQuery(document).on('click', '.choose_location', function() {
  
    var mystore = jQuery(this).attr("data-id");
    var distance = jQuery(this).attr("data-distance");
    var storename = jQuery(this).attr("data-storename");

    jQuery.cookie("preferred_store", null, { path: '/' });
    jQuery.cookie("preferred_distance", null, { path: '/' });
    jQuery.cookie("preferred_storename", null, { path: '/' });

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=choose_location&store_id=' + jQuery(this).attr("data-id") + '&distance=' + jQuery(this).attr("data-distance"),

        success: function(data) {

            jQuery(".header_store").html(data);
            jQuery.cookie("preferred_store", mystore, { expires: 1, path: '/' });
            jQuery.cookie("preferred_distance", distance, { expires: 1, path: '/' });
            jQuery.cookie("preferred_storename", storename, { expires: 1, path: '/' });
            setTimeout(addFlyerEvent, 1000);
            closeNav();
            iconsClick();
            jQuery(".contentFlyer").css('display', 'block');
            
        }
    });

});

// flyer close function 
function closeNav() {
    let storeLocation = document.getElementById("storeLocation");
    storeLocation.style.left = "-100%";
    storeLocation.classList.remove("ForOverlay");
}

// flyer event assign function
function addFlyerEvent() {
    var flyerOpener = document.getElementById('openFlyer');
    if(flyerOpener){
        flyerOpener.addEventListener("click", function(e) {
            let storeLocation = document.getElementById("storeLocation");
            storeLocation.style.left = "0";
            storeLocation.classList.add("ForOverlay");
        });
    }
   
}
function iconsClick(value){
    jQuery(document).ready(function(){
        jQuery(".locationWrapFlyer .icons i").click(function(){
            jQuery(".contentFlyer").animate({
                width: "toggle"
            });
        });
    });
}
iconsClick();
jQuery(document).ready(function() {

    jQuery(function($) {
        $('#storeLocation .choose_location').on('click', closeNav);

    });

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=get_storelisting',
        dataType: 'JSON',
        success: function(response) {
           
                var posts = JSON.parse(JSON.stringify(response));
                var header_data = posts.header;
                var list_data = posts.list;

                var mystore_loc =  jQuery.cookie("preferred_storename");
                
              
                jQuery(".header_store").html(header_data);
                setTimeout(addFlyerEvent, 1000);
                jQuery("#ajaxstorelisting").html(list_data);
                iconsClick();      
        }
    });

});

jQuery(document).ready(function() {

    var mystore_loc =  jQuery.cookie("preferred_storename");
    console.log(mystore_loc);
    jQuery(".populate-store select").val(mystore_loc);

});